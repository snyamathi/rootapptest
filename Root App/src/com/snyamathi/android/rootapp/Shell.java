package com.snyamathi.android.rootapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import android.util.Log;

public class Shell {

	private static final String TAG = Shell.class.getSimpleName();
	
    public static String sendCommand(String... cmd) {
    	Log.d(TAG, "Sending shell command: " + cmd);
        StringBuilder sb = new StringBuilder();
        try {
            String line;
            Process process = new ProcessBuilder(cmd).start();
            BufferedReader inputStream = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader errorStream = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            process.waitFor();

            while ((line = errorStream.readLine()) != null) {
                sb.append(line);
                sb.append('\n');
            }
            while ((line = inputStream.readLine()) != null) {
                sb.append(line);
                sb.append('\n');
                while ((line = errorStream.readLine()) != null) {
                    sb.append(line);
                    sb.append('\n');
                }
            }

            return sb.toString();
        } catch (InterruptedException e) {
			return e.getMessage();
		} catch (IOException e) {
			return e.getMessage();
		}
    }
}