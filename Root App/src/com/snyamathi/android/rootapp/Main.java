package com.snyamathi.android.rootapp;

import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.stericson.RootTools.Command;
import com.stericson.RootTools.RootTools;

public class Main extends Activity {

	private TextView tv;
	private com.stericson.RootTools.Shell shell;
	ArrayList<MyCommand> commands;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);       
		tv = (TextView) findViewById(R.id.textView1);

		if (RootTools.isRootAvailable() && RootTools.isAccessGiven()) {
			tv.setText("su is available and access is given");
		} else if (RootTools.isRootAvailable()) {
			tv.setText("su is available");
		} else {
			tv.setText("su is NOT available");
		}

		commands = new ArrayList<MyCommand>();

		try {
			shell = RootTools.getShell(true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		commands.add(new MyCommand(0, new String[] {"sendevent /dev/input/event0 3 53 737"}));
		commands.add(new MyCommand(1, new String[] {"sendevent /dev/input/event0 3 54 167"}));
		commands.add(new MyCommand(2, new String[] {"sendevent /dev/input/event0 3 58 70"}));
		commands.add(new MyCommand(3, new String[] {"sendevent /dev/input/event0 3 48 6"}));
		commands.add(new MyCommand(4, new String[] {"sendevent /dev/input/event0 3 57 0"}));
		commands.add(new MyCommand(5, new String[] {"sendevent /dev/input/event0 0 2 0"}));
		commands.add(new MyCommand(6, new String[] {"sendevent /dev/input/event0 0 0 0"}));
		commands.add(new MyCommand(7, new String[] {"sendevent /dev/input/event0 0 2 0"}));
		commands.add(new MyCommand(8, new String[] {"sendevent /dev/input/event0 0 0 0"}));


	}

	public void root(View view) {
		try {
			for (MyCommand c : commands) {
				shell.runRootCommand(c);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void click(View view) {
		System.out.println("click");
	}

	private class MyCommand extends Command {

		public MyCommand(int id, String[] command) {
			super(id, command);
		}

		@Override
		public void output(int arg0, String arg1) {
			// TODO Auto-generated method stub
		}

	}
}
